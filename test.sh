#!/bin/sh

# see also https://unix.stackexchange.com/a/687458

RSS_URL="https://alecthegeek.tech/blog/index.xml"

curl -sS "$RSS_URL" | xmllint --format --xpath "/rss/channel/item" - | head -10 | tee /tmp/test.size |
    sed -nEe '1i\
<!-- BLOG_FEED_START -->\
| Title | Description | Date |\
|-------|-------------|------|
/<!--\sraw\sHTML\somitted\s-->/s///g
\!<item><title>([^<]+)</title><link>([^<]+)</link><pubDate>[^,]+,\s([[:digit:]]{1,2}\s[[:alpha:]]+\s[[:digit:]]{4})\s[[:digit:]]{2}:[[:digit:]]{2}:[[:digit:]]{2}\s[+-][[:digit:]]{4}</pubDate><guid>[^<]+</guid><description>([^<]+)</description></item>$!s!!| [\1](https://alecthegeek.tech\2) | \4 | \3 |!p
$a\
<!-- BLOG_FEED_END -->' |
   sed -i -Ee '/BLOG_FEED_START/,/BLOG_FEED_END/ {r /dev/stdin
d}' README.md

[ -s /tmp/test.size ]  || echo "job failed" && exit 1
