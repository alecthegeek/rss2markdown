# Update a Git managed file with an RSS feed using GitLab CI/CD

## Overview

This CI/CD YAML file shows how you can use some shell tools and CI/CD "magic" to update
a project file (in this case a project README). The content being added is a Markdown table
created from the contents of an RSS feed.

It's based off work by @mlockhart-hax (See https://gitlab.com/mlockhart-hax/profile-update/)

This version uses a shell script instead of Ruby:

* xmllint is used to process the RSS feed and create lines of text
* sed then wrangles the result into Markdown
* a second sed command then inserts the content into the readme (location depends on BEGIN and END markers)

More info on setup below

## Here is the result

### A List of my last 10 blog posts

  <!-- BLOG_FEED_START -->
  | Title | Description | Date |
  |-------|-------------|------|
  <!-- BLOG_FEED_END -->

## Setup

You will need an RSS feed

1. Create a GitLab project token with read_repository and write_repository scope for the developer role
2. Make sure your default branch can be updated by the developer role
3. Check the destination file (in this case README.md) has the correct "BEGIN" and "END" markers so that sed script can add
   content in the correct location. See the README.md.backup file for an example
4. Modify the sed script to correctly parse your XML entries. This is the hard part, for my Hugo created feed it's this long sed command:

   ```sed
   \!<item><title>([^<]+)</title><link>([^<]+)</link><pubDate>[^,]+,\s([[:digit:]]{1,2}\s[[:alpha:]]+\s[[:digit:]]{4})\s[[:digit:]]{2}:[[:digit:]]{2}:[[:digit:]]{2}\s[+-][[:digit:]]{4}</pubDate><guid>[^<]+</guid><description>([^<]+)</description></item>$!s!!| [\1](https://alecthegeek.tech\2) | \4 | \3 |!p
   ```

5. TEST.

## Extra bonus content

1. This project needs a Linux image with xmllint, git, and curl installed. The CI/CD pipeline will build an image when the Dockerfile changes.
This image is then used to parse the XML and create the markdown.

1. This project uses [`glab`](https://docs.gitlab.com/ee/integration/glab/) (if installed) to verify the gitlab-ci.yml file during development. Note: The verification is skipped during the ci/cd job
   with the `--no-verify` flag on `push`). Run the command ` git config core.hooksPath "./.hooks"` to enable on your workstation