#!/bin/sh

docker container run --rm -it --mount "type=bind,source=$PWD,target=/work" --workdir /work registry.gitlab.com/alecthegeek/rss2markdown "$@"
